﻿using ElectronicObserver.Window.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ElectronicObserver.Window;
using ElectronicObserver.Resource.Record;
using ElectronicObserver.Observer;
using ElectronicObserver.Data;
using ElectronicObserver.Utility;

namespace RecordExt
{
	public class Plugin : ServerPlugin
	{
		ExpeditionRecord expeditionRecord = new ExpeditionRecord();

		public override string MenuTitle
		{
			get
			{
				return "记录扩展";
			}
		}

        public override string Version
        {
            get
            {
                return "1.0.0.1";
            }
        }

        public override bool RunService( FormMain main )
		{
			expeditionRecord.Load( RecordManager.Instance.MasterPath );

			autoSave = Configuration.Config.Log.AutoSave;
			autoSaveMinutes = Configuration.Config.Log.AutoSaveMinutes;
			Configuration.Instance.ConfigurationChanged += ConfigurationChanged;

			main.FormClosing += Main_FormClosing;
			SystemEvents.UpdateTimerTick += SystemEvents_UpdateTimerTick;

			APIObserver.Instance.APIList["api_req_mission/result"].ResponseReceived += Updated;

			return true;
		}

		private void ConfigurationChanged()
		{
			if ( !Configuration.Config.Log.AutoSave )
			{
				nowSeconds = 0;
				autoSave = false;
			}
			else
			{
				if ( !autoSave )
					expeditionRecord.Save( RecordManager.Instance.MasterPath );
				autoSave = true;
			}
		}


		private int nowSeconds = 0;
		private bool autoSave = false;
		private int autoSaveMinutes = 3 * 60;

		private void SystemEvents_UpdateTimerTick()
		{
			// 自动保存
			if ( autoSave && ++nowSeconds >= autoSaveMinutes * 60 )
			{
				expeditionRecord.Save( RecordManager.Instance.MasterPath );
				nowSeconds = 0;
			}
		}

		private void Updated( string apiname, dynamic data )
		{

			var item = new ExpeditionRecord.ExpeditionElement();
			item.ExpeditionName = data.api_quest_name;
			item.Result = Constants.GetExpeditionResult( (int)data.api_clear_result );
			item.HQExp = (int)data.api_get_exp;
			item.ShipExp = ( (int[])data.api_get_ship_exp ).Min();
			try
			{
				item.Material = (int[])data.api_get_material;
			}
			catch
			{
				item.Material = new int[4];
			}

			int[] flags = (int[])data.api_useitem_flag;
			string name = data.api_quest_name;
			MissionData master_mission = KCDatabase.Instance.Mission.Values.FirstOrDefault( m => m.Name == name );
			string item1name = null;
			string item2name = null;
			if ( master_mission != null )
			{
				int item1id = ( (int[])master_mission.RawData.api_win_item1 )[0];
				int item2id = ( (int[])master_mission.RawData.api_win_item2 )[0];
				if ( item1id > 0 )
					item1name = KCDatabase.Instance.MasterUseItems[item1id].Name;
				if ( item2id > 0 )
					item2name = KCDatabase.Instance.MasterUseItems[item2id].Name;
			}

			if ( flags[0] > 0 && data.api_get_item1() )
			{

				item.Item1Name = item1name ?? data.api_get_item1.api_useitem_name;
				item.Item1Count = (int)data.api_get_item1.api_useitem_count;
			}
			if ( flags[1] > 0 && data.api_get_item2() )
			{
				item.Item2Name = item2name ?? data.api_get_item2.api_useitem_name;
				item.Item2Count = (int)data.api_get_item2.api_useitem_count;
			}

			expeditionRecord.Record.Add( item );

		}

		private void Main_FormClosing( object sender, System.Windows.Forms.FormClosingEventArgs e )
		{
			expeditionRecord.Save( RecordManager.Instance.MasterPath );
		}
	}
}
